<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableHelpreports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helpreports', function (Blueprint $table) {
            $table->increments("id");
            $table->integer("user_id")->unsigned();
            $table->date("tanggal");
            $table->string("alamat");
            $table->string("jenis_kerusakan");
            $table->string("jenis_bantuan");
            $table->string("telepon");
            $table->string("keterangan");
            $table->string("foto");
            $table->string("latitude");
            $table->string("longitude");
            $table->enum("status", ["BARU","PROSES","SELESAI","CANCEL"]);
            $table->integer("created_by")->nullable();
            $table->integer("updated_by")->nullable();
            $table->integer("deleted_by")->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users");
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('helpreports');

    }
}
