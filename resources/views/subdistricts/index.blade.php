@extends("layouts.global")
@section("title") Data Kecamatan @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row m-2 ">
            <div class="col-lg-12 text-center">
                <h1>Data Kecamatan</h1>
            </div><br><br>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item active"><a href="{{route('subdistricts.index')}}">Data Aktif</a></li>
                    <li class="breadcrumb-item"><a href="{{route('subdistricts.trash')}}">Data Arsip</a></li>
                </ol>
            </div>
            <div class="col-sm-6 text-right">
                <a href="{{route('subdistricts.create')}}" class="btn btn-primary btn-sm">Tambah</a>
            </div>
        </div>
        @if(session('status'))
        <div class="alert">
            {{session('status')}}
        </div>
        @endif
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">

                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th>Nama subdistrict</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($subdistricts as $subdistrict)
                                <tr>
                                    <td>{{$subdistrict->name}}</td>
                                    <td>{{$subdistrict->latitude}}</td>
                                    <td>{{$subdistrict->longitude}}</td>
                                    <td>
                                        <a class="btn btn-primary btn-sm"
                                            href="{{route('subdistricts.edit',['subdistrict'=>$subdistrict->id])}}">Edit</a>
                                        <form
                                            onsubmit="return confirm('Apakah Anda yakin ingin mengarsipkan data ini ?')"
                                            class="d-inline"
                                            action="{{route('subdistricts.destroy', ['subdistrict' => $subdistrict->id ])}}"
                                            method="POST">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="submit" value="Arsip" class="btn btn-warning btn-sm">
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colSpan="10">
                                        {{$subdistricts->appends(Request::all())->links()}}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection