@extends("layouts.global")
@section("title") Edit Data Kecamatan @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2"> 
        </div>     
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <form enctype="multipart/form-data" class="bg-white shadow-sm p-10"
            action="{{route('subdistricts.update', ['subdistrict'=>$subdistrict->id])}}" method="POST">
            <div class="card-body table-responsive p-10">
                @csrf
                <input type="hidden" value="PUT" name="_method">
                <div class="form-group">
                    <label for="name">Nama Kecamatan</label>
                    <input value="{{$subdistrict->name}}" type="text" class="form-control" id="name" name="name"
                        placeholder="Masukkan Nama Kecamatan">
                </div>
                <div class="form-group">
                    <label for="latitude">Latitude</label>
                    <input value="{{$subdistrict->latitude}}" type="text" class="form-control" id="latitude"
                        name="latitude" placeholder="Masukkan posisi latitude kecamatan">
                </div>
                <div class="form-group">
                    <label for="longitude">Longitude</label>
                    <input value="{{$subdistrict->longitude}}" type="text" class="form-control" id="longitude"
                        name="longitude" placeholder="Masukkan posisi longitude kecamatan">
                </div>
                <button type="submit" value="Save" class="btn btn-primary">Edit</button>
            </div>
        </form>
    </div>
</section>
@endsection