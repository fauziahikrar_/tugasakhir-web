@extends("layouts.global")
@section("title") Tambah Data Kecamatan @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <form enctype="multipart/form-data" class="bg-white shadow-sm p-10" action="{{route('subdistricts.store')}}"
            method="POST">
            <div class="card-body table-responsive p-10">
                @csrf
                <div class="form-group">
                    <label for="name">Nama Kecamatan</label>
                    <input value="{{old('name')}}"
                        class="form-control {{$errors->first('name') ? "is-invalid" : ""}}" placeholder="Masukkan Nama Kecamatan"
                        type="text" name="name" id="name" />
                    <div class="invalid-feedback">
                        {{$errors->first('name')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="latitude">Latitude</label>
                        <input value="{{old('latitude')}}" class="form-control {{$errors->first('latitude') ? "is-invalid" : ""}}"
                        placeholder="Masukkan posisi latitude kecamatan" type="text" name="latitude" id="latitude" />
                    <div class="invalid-feedback">
                        {{$errors->first('latitude')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="longitude">Longitude</label>
                        <input value="{{old('longitude')}}" class="form-control {{$errors->first('longitude') ? "is-invalid" : ""}}"
                        placeholder="Masukkan posisi longitude kecamatan" type="text" name="longitude" id="longitude" />
                    <div class="invalid-feedback">
                        {{$errors->first('longitude')}}
                    </div> 
                </div>
                <button type="submit" value="Save" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</section>
@endsection