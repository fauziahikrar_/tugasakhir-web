@extends("layouts.global")
@section("title") Data Arsip Laporan Banjir @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row m-2 ">
            <div class="col-lg-12 text-center">
                <h1>Data Laporan Banjir</h1>
            </div><br><br>
            <div class="col-sm-10">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="{{route('floodreports.index')}}">Data Aktif</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'baru' ?'active' : '' }}"
                            href="{{route('floodreports.index', ['status' =>'baru'])}}">Data Baru</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'proses' ? 'active' : '' }}"
                            href="{{route('floodreports.index', ['status' => 'proses'])}}">Data Diproses</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'selesai' ?'active' : '' }}"
                            href="{{route('floodreports.index', ['status' =>'selesai'])}}">Data Terverifikasi</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'cancel' ?'active' : '' }}"
                            href="{{route('floodreports.index', ['status' =>'cancel'])}}">Data Dicancel</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::path() == 'floodreports/trash' ? 'active' :''}}"
                            href="{{route('floodreports.trash')}}">Data Arsip</a></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">

                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th><b>Nama Pelapor</b></th>
                                    <th><b>Tanggal</b></th>
                                    <th><b>Alamat</b></th>
                                    <th><b>Tinggi genangan</b></th>
                                    <th><b>Keterangan</b></th>
                                    <th><b>Foto</b></th>
                                    <th><b>Latitude</b></th>
                                    <th><b>Longitude</b></th>
                                    <th><b>Status</b></th>
                                    <th><b>Opsi</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($floodreports as $floodreport)
                                <tr>
                                    <td>{{$floodreport->user['username']}}</td>
                                    <td>{{$floodreport->tanggal}}</td>
                                    <td>{{$floodreport->alamat}}</td>
                                    <td>{{$floodreport->tinggi_genangan}}</td>
                                    <td>{{$floodreport->keterangan}}</td>
                                    <td>
                                        @if($floodreport->foto)
                                        <img src="{{asset('storage/' . $floodreport->foto)}}" width="50px" />
                                        @endif
                                    </td>
                                    <td>{{$floodreport->latitude}}</td>
                                    <td>{{$floodreport->longitude}}</td>
                                    <td>
                                        @if($floodreport->status == "BARU")
                                        <span class="badge bg-warning text-light">{{$floodreport->status}}</span>
                                        @elseif($floodreport->status == "PROSES")
                                        <span class="badge bg-info text-light">{{$floodreport->status}}
                                        </span>
                                        @elseif($floodreport->status == "SELESAI")
                                        <span class="badge bg-success text-light">{{$floodreport->status}}</span>
                                        @elseif($floodreport->status == "CANCEL")
                                        <span class="badge bg-dark text-light">{{$floodreport->status}}
                                        </span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('floodreports.restore', ['id' => $floodreport->id])}}"
                                            class="btn btn-success btn-sm">Restore</a>
                                        <form class="d-inline"
                                            action="{{route('floodreports.delete-permanent', ['id' => $floodreport->id])}}"
                                            method="POST"
                                            onsubmit="return confirm('Apakah Anda yakin ingin menghapus data ini secara permanen?')">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <input type="submit" class="btn btn-danger btn-sm" value="Delete" />
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colSpan="10">
                                        {{$floodreports->appends(Request::all())->links()}}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
@endsection