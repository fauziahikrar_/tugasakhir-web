@extends("layouts.global")
@section("title") Edit Data Laporan Banjir @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <form enctype="multipart/form-data" class="bg-white shadow-sm p-10"
            action="{{route('floodreports.update', ['floodreport'=>$floodreport->id])}}" method="POST">
            <div class="card-body table-responsive p-10">
                @csrf
                <input type="hidden" value="PUT" name="_method">
                <div class="form-group">
                    <label for="tanggal">Tanggal Laporan</label>
                    <input value="{{$floodreport->tanggal}}" type="date" class="form-control" id="tanggal" name="tanggal"
                        placeholder="Masukkan tanggal">
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat Laporan</label>
                    <input value="{{$floodreport->alamat}}" type="text" class="form-control" id="alamat" name="alamat"
                        placeholder="Masukkan Alamat">
                </div>
                <div class="form-group">
                    <label for="tinggi_genangan">Tinggi Genangan</label>
                    <input value="{{$floodreport->tinggi_genangan}}" type="text" class="form-control" id="tinggi_genangan"
                        name="tinggi_genangan" placeholder="Masukkan tinggi genangan banjir">
                </div>
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <input value="{{$floodreport->keterangan}}" type="text" class="form-control" id="keterangan"
                        name="keterangan" placeholder="Masukkan Keterangan">
                </div>
                <label for="foto">Foto</label><br>
                <small class="text-muted">Foto Awal</small><br>
                @if($floodreport->foto)
                <img src="{{asset('storage/' . $floodreport->foto)}}" width="50px" />
                @endif
                <br><br>
                <input type="file" class="form-control" name="foto">
                <small class="text-muted">Kosongkan jika tidak ingin mengubah
                    Foto</small>
                <div class="form-group">
                    <label for="latitude">Latitude</label>
                    <input value="{{$floodreport->latitude}}" type="text" class="form-control" id="latitude" name="latitude"
                        placeholder="Masukkan posisi latitude Desa">
                </div>
                <div class="form-group">
                    <label for="longitude">Longitude</label>
                    <input value="{{$floodreport->longitude}}" type="text" class="form-control" id="longitude"
                        name="longitude" placeholder="Masukkan posisi longitude Desa">
                </div>
                <div class="form-group">
                <label for="status">Status</label><br>
                <select class="form-control" name="status" id="status">
                    <option {{$floodreport->status == "BARU" ? "selected" : ""}} value="BARU">BARU</option>
                    <option {{$floodreport->status == "PROSES" ? "selected" : ""}} value="PROSES">PROSES</option>
                    <option {{$floodreport->status == "SELESAI" ? "selected" : ""}} value="SELESAI">SELESAI</option>
                    <option {{$floodreport->status == "CANCEL" ? "selected" : ""}} value="CANCEL">CANCEL</option>
                </select>
                </div>
                <button type="submit" value="Save" class="btn btn-primary">Edit</button>
            </div>
        </form>
    </div>
</section>
@endsection