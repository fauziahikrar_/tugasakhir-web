@extends("layouts.global")
@section("title") Edit Data Desa @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2"> 
        </div>     
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <form enctype="multipart/form-data" class="bg-white shadow-sm p-10"
            action="{{route('villages.update', ['village'=>$village->id])}}" method="POST">
            <div class="card-body table-responsive p-10">
                @csrf
                <input type="hidden" value="PUT" name="_method">
                <div class="form-group">
                    <label for="name">Nama Desa</label>
                    <input value="{{$village->name}}" type="text" class="form-control" id="name" name="name"
                        placeholder="Masukkan Nama Desa">
                </div>
                <div class="form-group">
                    <label for="subdistricts">Kecamatan</label><br>
                    <select name="subdistricts" class="form-control">
                    @foreach($subdistrict as $key =>$item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="latitude">Latitude</label>
                    <input value="{{$village->latitude}}" type="text" class="form-control" id="latitude"
                        name="latitude" placeholder="Masukkan posisi latitude Desa">
                </div>
                <div class="form-group">
                    <label for="longitude">Longitude</label>
                    <input value="{{$village->longitude}}" type="text" class="form-control" id="longitude"
                        name="longitude" placeholder="Masukkan posisi longitude Desa">
                </div>
                <button type="submit" value="Save" class="btn btn-primary">Edit</button>
            </div>
        </form>
    </div>
</section>
@endsection