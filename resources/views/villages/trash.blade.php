@extends("layouts.global")
@section("title") Data Arsip Desa @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row m-2 ">
            <div class="col-lg-12 text-center">
                <h1>Data Desa</h1>
            </div><br><br>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="{{route('villages.index')}}">Data Aktif</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('villages.trash')}}">Data Arsip</a></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">

                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th>Nama Desa</th>
                                    <th>Nama Kecamatan</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($villages as $village)
                                <tr>
                                    <td>{{$village->name}}</td>
                                    <td>{{$village->subdistrict['name'] }}</td>
                                    <td>{{$village->latitude}}</td>
                                    <td>{{$village->longitude}}</td>
                                    <td>
                                        <a href="{{route('villages.restore', ['id' => $village->id])}}"
                                            class="btn btn-success btn-sm">Restore</a>
                                        <form class="d-inline"
                                            action="{{route('villages.delete-permanent', ['id' => $village->id])}}"
                                            method="POST"
                                            onsubmit="return confirm('Apakah Anda yakin ingin menghapus data ini secara permanen?')">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <input type="submit" class="btn btn-danger btn-sm" value="Delete" />
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colSpan="10">
                                        {{$villages->appends(Request::all())->links()}}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
@endsection