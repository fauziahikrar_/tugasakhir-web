@extends("layouts.global")
@section("title") Tambah Data Posko @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <form enctype="multipart/form-data" class="bg-white shadow-sm p-10" action="{{route('posts.store')}}"
            method="POST">
            <div class="card-body table-responsive p-10">
                @csrf
                <div class="form-group">
                    <label for="name">Nama Posko</label>
                    <input value="{{old('name')}}" class="form-control {{$errors->first('name') ? "is-invalid" : ""}}"
                        placeholder="Masukkan nama posko" type="text" name="name" id="name" />
                    <div class="invalid-feedback">
                        {{$errors->first('name')}}
                    </div>    
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat Posko </label>
                    <input value="{{old('alamat')}}" class="form-control {{$errors->first('alamat') ? "is-invalid" : ""}}"
                        placeholder="Masukkan alamat posko" type="text" name="alamat" id="alamat" />
                    <div class="invalid-feedback">
                        {{$errors->first('alamat')}}
                    </div>  
                </div>
                <div class="form-group">
                    <label for="villages">Desa</label><br>
                    <select name="villages" class="form-control">
                        @foreach($village as $key =>$item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="deskripsi">Deskripsi</label>
                    <input value="{{old('deskripsi')}}" class="form-control {{$errors->first('deskripsi') ? "is-invalid" : ""}}"
                        placeholder="Masukkan deskripsi" type="text" name="deskripsi" id="deskripsi" />
                    <div class="invalid-feedback">
                        {{$errors->first('deskripsi')}}
                    </div> 
                </div>
                <div class="form-group">
                    <label for="latitude">Latitude</label>
                        <input value="{{old('latitude')}}" class="form-control {{$errors->first('latitude') ? "is-invalid" : ""}}"
                        placeholder="Masukkan posisi latitude posko" type="text" name="latitude" id="latitude" />
                    <div class="invalid-feedback">
                        {{$errors->first('latitude')}}
                    </div>   
                </div>
                <div class="form-group">
                    <label for="longitude">Longitude</label>
                        <input value="{{old('longitude')}}" class="form-control {{$errors->first('longitude') ? "is-invalid" : ""}}"
                        placeholder="Masukkan posisi longitude posko" type="text" name="longitude" id="longitude" />
                    <div class="invalid-feedback">
                        {{$errors->first('longitude')}}
                    </div> 
                </div>
                <button type="submit" value="Save" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</section>
@endsection