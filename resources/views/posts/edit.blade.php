@extends("layouts.global")
@section("title") Edit Data Posko @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <form enctype="multipart/form-data" class="bg-white shadow-sm p-10"
            action="{{route('posts.update', ['post'=>$post->id])}}" method="POST">
            <div class="card-body table-responsive p-10">
                @csrf
                <input type="hidden" value="PUT" name="_method">
                <div class="form-group">
                    <label for="name">Nama Posko</label>
                    <input value="{{$post->name}}" type="text" class="form-control" id="name" name="name"
                        placeholder="Masukkan nama posko">
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat Posko</label>
                    <input value="{{$post->alamat}}" type="text" class="form-control" id="alamat" name="alamat"
                        placeholder="Masukkan Alamat">
                </div>
                <div class="form-group">
                    <label for="villages">Desa</label><br>
                    <select name="villages" class="form-control">
                        @foreach($village as $key =>$item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="deskripsi">Deskripsi</label>
                    <input value="{{$post->deskripsi}}" type="text" class="form-control" id="deskripsi" name="deskripsi"
                        placeholder="Masukkan deskripsi posko">
                </div> 
                <div class="form-group">
                    <label for="latitude">Latitude</label>
                    <input value="{{$post->latitude}}" type="text" class="form-control" id="latitude" name="latitude"
                        placeholder="Masukkan posisi latitude Desa">
                </div>
                <div class="form-group">
                    <label for="longitude">Longitude</label>
                    <input value="{{$post->longitude}}" type="text" class="form-control" id="longitude"
                        name="longitude" placeholder="Masukkan posisi longitude Desa">
                </div>
                <button type="submit" value="Save" class="btn btn-primary">Edit</button>
            </div>
        </form>
    </div>
</section>
@endsection