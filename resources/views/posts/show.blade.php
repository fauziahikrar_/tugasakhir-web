@extends("layouts.global")
@section("title") Detail Posko @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row m-2 ">
            <div class="col-lg-12 text-center">
                <h1>Posko {{$post->name}}</h1>
            </div><br><br>

        </div>
        @if(session('status'))
        <div class="alert">
            {{session('status')}}
        </div>
        @endif
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card col-6 ">
            <div class="row col-12">
                <div class="col-sm-6 ">
                    <label>Deskripsi</label>
                </div>
                <div class="col-sm-6 ">
                    <label>{{$post->deskripsi}}</label>
                </div>            
               <div class="col-sm-6 ">
                    <label>Latitude</label>
                </div>
                <div class="col-sm-6 ">
                    <label>{{$post->latitude}}</label>
                </div>
                <div class="col-sm-6 ">
                    <label>Longitude</label>
                </div>
                <div class="col-sm-6 ">
                    <label>{{$post->longitude}}</label>
                </div>
            </div><br>
        </div>
    </div>
</section>
@endsection