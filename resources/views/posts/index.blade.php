@extends("layouts.global")
@section("title") Data Posko @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row m-2 ">
            <div class="col-lg-12 text-center">
                <h1>Data Posko</h1>
            </div><br><br>
            <div class="col-sm-10">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item active"><a href="{{route('posts.index')}}">Data Aktif</a></li>
                    <li class="breadcrumb-item"><a href="{{route('posts.trash')}}">Data Arsip</a></li>
                </ol>
            </div>
            <div class="col-sm-2 text-right">
                <a href="{{route('posts.create')}}" class="btn btn-primary btn-sm">Tambah</a>
            </div>
        </div>
        @if(session('status'))
        <div class="alert">
            {{session('status')}}
        </div>
        @endif
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th><b>Nama Posko</b></th>
                                    <th><b>Alamat</b></th>
                                    <th><b>Desa</b></th>
                                    <th><b>Kecamatan</b></th>
                                    <th><b>Opsi</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($posts as $post)
                                <tr>
                                    <td>{{$post->name}}</td>
                                    <td>{{$post->alamat}}</td>
                                    <td>{{$post->village['name']}}</td>
                                    <td>{{$post->village->subdistrict['name']}}</td>
                                    <td>
                                        <a class="btn btn-primary btn-sm"
                                            href="{{route('posts.edit',['post'=>$post->id])}}">Edit</a>
                                        <a class="btn btn-success btn-sm"
                                            href="{{route('posts.show',['post'=>$post->id])}}">Info</a>
                                        <form
                                            onsubmit="return confirm('Apakah Anda yakin ingin mengarsipkan data ini ?')"
                                            class="d-inline" action="{{route('posts.destroy', ['post' => $post->id ])}}"
                                            method="POST">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="submit" value="Arsip" class="btn btn-warning btn-sm">
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colSpan="10">
                                        {{$posts->appends(Request::all())->links()}}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection