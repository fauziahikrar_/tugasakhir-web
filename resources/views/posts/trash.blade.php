@extends("layouts.global")
@section("title") Data Arsip Posko @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row m-2 ">
            <div class="col-lg-12 text-center">
                <h1>Data Posko</h1>
            </div><br><br>
            <div class="col-sm-10">
            <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item "><a href="{{route('floodhistories.index')}}">Data Aktif</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('floodhistories.trash')}}">Data Arsip</a></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">

                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th><b>Nama Posko</b></th>
                                    <th><b>Alamat</b></th>
                                    <th><b>Desa</b></th>
                                    <th><b>Kecamatan</b></th>
                                    <th><b>Deskripsi</b></th>
                                    <th><b>Latitude</b></th>
                                    <th><b>Longitude</b></th>
                                    <th><b>Opsi</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($posts as $post)
                                <tr>
                                    <td>{{$post->name}}</td>
                                    <td>{{$post->alamat}}</td>
                                    <td>{{$post->village['name']}}</td>
                                    <td>{{$post->village->subdistrict['name']}}</td>
                                    <td>{{$post->deskripsi}}</td>
                                    <th><b>{{$post->latitude}}</b></th>
                                    <th><b>{{$post->longitude}}</b></th>
                                    <td>
                                        <a href="{{route('posts.restore', ['id' => $post->id])}}"
                                            class="btn btn-success btn-sm">Restore</a>
                                        <form class="d-inline"
                                            action="{{route('posts.delete-permanent', ['id' => $post->id])}}"
                                            method="POST"
                                            onsubmit="return confirm('Apakah Anda yakin ingin menghapus data ini secara permanen?')">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <input type="submit" class="btn btn-danger btn-sm" value="Delete" />
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colSpan="10">
                                        {{$posts->appends(Request::all())->links()}}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
@endsection