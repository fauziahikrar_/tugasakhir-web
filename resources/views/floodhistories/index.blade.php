@extends("layouts.global")
@section("title") Data Riwayat Banjir @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row m-2 ">
            <div class="col-lg-12 text-center">
                <h1>Data Riwayat Banjir</h1>
            </div><br><br>
            <div class="col-sm-10">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item active"><a href="{{route('floodhistories.index')}}">Data Aktif</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'baru' ?'active' : '' }}"
                            href="{{route('floodhistories.index', ['status' =>'baru'])}}">Data Baru</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'selesai' ?'active' : '' }}"
                            href="{{route('floodhistories.index', ['status' =>'selesai'])}}">Data Selesai</a></li>
                    <li class="breadcrumb-item"><a href="{{route('floodhistories.trash')}}">Data Arsip</a></li>
                </ol>
            </div>
            <div class="col-sm-2 text-right">
                <a href="{{route('floodhistories.create')}}" class="btn btn-primary btn-sm">Tambah</a>
            </div>
        </div>
        @if(session('status'))
        <div class="alert">
            {{session('status')}}
        </div>
        @endif
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th><b>Tanggal</b></th>
                                    <th><b>Desa</b></th>
                                    <th><b>Kecamatan</b></th>
                                    <th><b>Status</b></th>
                                    <th><b>Opsi</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($floodhistories as $floodhistory)
                                <tr>
                                    <td>{{$floodhistory->tanggal}}</td>
                                    <td>{{$floodhistory->village['name']}}</td>
                                    <td>{{$floodhistory->village->subdistrict['name']}}</td>
                                    <td>
                                        @if($floodhistory->status == "BARU")
                                        <span class="badge bg-warning text-light">{{$floodhistory->status}}</span>
                                        @elseif($floodhistory->status == "SELESAI")
                                        <span class="badge bg-success text-light">{{$floodhistory->status}}</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-primary btn-sm"
                                            href="{{route('floodhistories.edit',['floodhistory'=>$floodhistory->id])}}">Edit</a>
                                        <a class="btn btn-success btn-sm"
                                            href="{{route('floodhistories.show',['floodhistory'=>$floodhistory->id])}}">Info</a>
                                        <form
                                            onsubmit="return confirm('Apakah Anda yakin ingin mengarsipkan data ini ?')"
                                            class="d-inline"
                                            action="{{route('floodhistories.destroy', ['floodhistory' => $floodhistory->id ])}}"
                                            method="POST">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="submit" value="Arsip" class="btn btn-warning btn-sm">
                                        </form>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colSpan="10">
                                        {{$floodhistories->appends(Request::all())->links()}}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection