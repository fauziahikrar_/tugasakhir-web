@extends("layouts.global")
@section("title") Edit Data Riwayat Banjir @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <form enctype="multipart/form-data" class="bg-white shadow-sm p-10"
            action="{{route('floodhistories.update', ['floodhistory'=>$floodhistory->id])}}" method="POST">
            <div class="card-body table-responsive p-10">
                @csrf
                <input type="hidden" value="PUT" name="_method">
                <div class="form-group">
                    <label for="tanggal">Tanggal Kejadian</label>
                    <input value="{{$floodhistory->tanggal}}" type="date" class="form-control" id="tanggal" name="tanggal"
                        placeholder="Masukkan tanggal">
                </div>
                <div class="form-group">
                    <label for="villages">Desa</label><br>
                    <select name="villages" class="form-control">
                        @foreach($village as $key =>$item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="kepala_keluarga">Jumlah Kepala Keluarga Terdampak</label>
                    <input value="{{$floodhistory->kepala_keluarga}}" type="text" class="form-control" id="kepala_keluarga" name="kepala_keluarga"
                        placeholder="Masukkan Jumlah Kepala Keluarga">
                </div>
                <div class="form-group">
                    <label for="jiwa">Jumlah Jiwa Terdampak</label>
                    <input value="{{$floodhistory->jiwa}}" type="number" class="form-control" id="jiwa" name="jiwa"
                        placeholder="Masukkan jumlah jiwa terdampak banjir">
                </div>
                <div class="form-group">
                    <label for="rumah">Jumlah Rumah Terdampak</label>
                    <input value="{{$floodhistory->rumah}}" type="number" class="form-control" id="rumah" name="rumah"
                        placeholder="Masukkan jumlah rumah terdampak banjir">
                </div>
                <div class="form-group">
                    <label for="sekolah">Jumlah Sekolah Terdampak</label>
                    <input value="{{$floodhistory->sekolah}}" type="number" class="form-control" id="sekolah" name="sekolah"
                        placeholder="Masukkan jumlah sekolah terdampak banjir">
                </div>
                <div class="form-group">
                    <label for="kantor_desa">Jumlah Kantor Desa Terdampak</label>
                    <input value="{{$floodhistory->kantor_desa}}" type="number" class="form-control" id="kantor_desa" name="kantor_desa"
                        placeholder="Masukkan jumlah kantor desa terdampak banjir">
                </div>
                <div class="form-group">
                    <label for="sawah">Luas Sawah Terdamapak</label>
                    <input value="{{$floodhistory->sawah}}" type="text" class="form-control" id="sawah" name="sawah"
                        placeholder="Masukkan luas sawah terdampak banjir">
                </div>
                <div class="form-group">
                    <label for="jalan">TInggi genangan air di jalan</label>
                    <input value="{{$floodhistory->jalan}}" type="text" class="form-control" id="jalan" name="jalan"
                        placeholder="Masukkan tinggi genangan air di jalan">
                </div>
                <div class="form-group">
                    <label for="latitude">Latitude</label>
                    <input value="{{$floodhistory->latitude}}" type="text" class="form-control" id="latitude" name="latitude"
                        placeholder="Masukkan posisi latitude Banjir">
                </div>
                <div class="form-group">
                    <label for="longitude">Longitude</label>
                    <input value="{{$floodhistory->longitude}}" type="text" class="form-control" id="longitude"
                        name="longitude" placeholder="Masukkan posisi longitude Banjir">
                </div>
                <div class="form-group">
                <label for="status">Status</label><br>
                <select class="form-control" name="status" id="status">
                    <option {{$floodhistory->status == "BARU" ? "selected" : ""}} value="BARU">BARU</option>
                    <option {{$floodhistory->status == "SELESAI" ? "selected" : ""}} value="SELESAI">SELESAI</option>
                </select>
                </div>
                
                <button type="submit" value="Save" class="btn btn-primary">Edit</button>
            </div>
        </form>
    </div>
</section>
@endsection