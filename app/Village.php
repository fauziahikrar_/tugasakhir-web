<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Village extends Model
{
    protected $table = 'villages';

    use SoftDeletes;

    public function subdistrict(){
        return $this->belongsTo('App\Subdistrict');
    }

    public function floodhistories(){
        return $this->hasMany('App\FloodHistory');
    }


    public function posts(){
        return $this->hasMany('App\Post');
    }
}
