<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Subdistrict extends Model
{
    protected $table = 'subdistricts';

    use SoftDeletes;

    public function villages(){
        return $this->hasMany('App\Village');
    }
    
}