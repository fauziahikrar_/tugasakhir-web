<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class HelpReport extends Model
{
    protected $table = 'helpreports';

    use SoftDeletes;

    public function user(){
        return $this->belongsTo('App\User');
    }

    protected $fillable = [
        'user_id','tanggal', 'alamat', 'jenis_kerusakan', 'jenis_bantuan', 'telepon', 'keterangan','foto', 'latitude', 'longitude', 'status'
    ];
    
}
