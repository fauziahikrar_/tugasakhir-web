<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class FloodReport extends Model
{
    protected $table = 'floodreports';

    use SoftDeletes;

    public function user(){
        return $this->belongsTo('App\User');
    }

    protected $fillable = [
        'tanggal', 'alamat', 'tinggi_genangan', 'foto', 'user_id', 'keterangan', 'latitude', 'longitude', 'status'
    ];

   
}
