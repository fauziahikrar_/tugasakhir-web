<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class FloodReportController extends Controller
{

    public function __construct(){
        $this->middleware(function($request, $next){
            if(Gate::allows('manage-operator')) return $next($request);
            abort(403, 'Anda tidak memiliki cukup hak akses');
            });
        }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->get('status');

        $keyword = $request->get('keyword') ? $request->get('keyword') : '';
        
        if($status){
            $floodreports = \App\Floodreport::where('status', strtoupper($status))->paginate(10);
        } else {
            $floodreports = \App\Floodreport::paginate(10);
        }

        return view('floodreports.index', ['floodreports' => $floodreports]);
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view('floodreports.create'); 
  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_floodreport = new \App\Floodreport;
        $new_floodreport->tanggal = $request->get('tanggal');
        $new_floodreport->alamat = $request->get('alamat');
        $new_floodreport->tinggi_genangan = $request->get('tinggi_genangan');
        $new_floodreport->keterangan = $request->get('keterangan');

        $foto = $request->file('foto');
        if($foto){
            $foto_path = $foto->store('floodreport-fotos', 'public');
            $new_floodreport->foto = $foto_path;
        }

        $new_floodreport->latitude = $request->get('latitude');
        $new_floodreport->longitude = $request->get('longitude');
        $new_floodreport->status = $request->get('status');

        $new_floodreport->user_id = \Auth::user()->id;
        $new_floodreport->created_by = \Auth::user()->id;
        $new_floodreport->save();

        return redirect()->route('floodreports.index')->with('status', 'Data laporan banjir telah berhasil dibuat');
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $floodreport = \App\Floodreport::findOrFail($id);
        

        return view('floodreports.show', ['floodreport' => $floodreport]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $floodreport_to_edit = \App\Floodreport::findOrFail($id);
        
        return view('floodreports.edit', ['floodreport' => $floodreport_to_edit]);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $floodreport = \App\Floodreport::findOrFail($id);

        $floodreport->tanggal = $request->get('tanggal');
        $floodreport->alamat = $request->get('alamat');
        $floodreport->tinggi_genangan = $request->get('tinggi_genangan');
        $floodreport->keterangan = $request->get('keterangan');
           
        $new_foto = $request->file('foto');

        if($new_foto){
            if($floodreport->foto && file_exists(storage_path('app/public/' . $floodreport->foto))){
            \Storage::delete('public/'. $floodreport->foto);
        }
        $new_foto_path = $new_foto->store('floodreport-fotos', 'public');
        
        $floodreport->foto = $new_foto_path;
        }

        $floodreport->latitude = $request->get('latitude');
        $floodreport->longitude = $request->get('longitude');
        $floodreport->status = $request->get('status');

        $floodreport->updated_by = \Auth::user()->id;

        $floodreport->save();

        return redirect()->route('floodreports.index', ['id'=>$floodreport->id])->with('status', 'Laporan banjir berhasil diperbarui');
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $floodreport = \App\Floodreport::findOrFail($id);

        $floodreport->delete();

        return redirect()->route('floodreports.index')->with('status', 'Laporan telah diarsipkan');
 
    }

    public function trash(){
        $deleted_floodreport = \App\floodreport::onlyTrashed()->paginate(10);
        
        return view('floodreports.trash', ['floodreports' => $deleted_floodreport]);
    }

    public function restore($id){
        $floodreport = \App\Floodreport::withTrashed()->findOrFail($id);
        
        if($floodreport->trashed()){
            $floodreport->restore();
        } else {
            return redirect()->route('floodreports.trash')->with('status', 'Laporan tidak ada di tabel arsip');
        }

        return redirect()->route('floodreports.trash')->with('status', 'Laporan telah berhasil direstore');
    }

    public function deletePermanent($id){
        $floodreport = \App\Floodreport::withTrashed()->findOrFail($id);
        
        if(!$floodreport->trashed()){
            return redirect()->route('floodreports.trash')->with('status', 'Laporan aktif, Tidak bisa hapus permanen!');
        } else {
            $floodreport->forceDelete();
            return redirect()->route('floodreports.trash')->with('status', 'Laporan telah berhasil dihapus permanen');
        }
    }   
}
