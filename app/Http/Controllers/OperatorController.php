<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class OperatorController extends Controller
{

    public function __construct(){
        $this->middleware(function($request, $next){
            if(Gate::allows('manage-operator')) return $next($request);
            abort(403, 'Anda tidak memiliki cukup hak akses');
            });
        }

    public function index()
    {
        return view('home');
    }
}
