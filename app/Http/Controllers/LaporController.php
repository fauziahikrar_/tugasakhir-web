<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LaporController extends Controller
{

    public function __construct(){
        $this->middleware(function($request, $next){
            if(Gate::allows('manage-lapor')) return $next($request);
            abort(403, 'Anda tidak memiliki cukup hak akses');
            });
        }

    public function index()
    {
        return view('home');
    }
}
