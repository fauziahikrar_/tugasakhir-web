<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{

    public function action(){

        $data = Post::all();

        $params = [
            'code' => 201,
            'message' => 'Get Posko Evakuasi Success!',
            'dataposko' => $data,
        ];
        return response()->json($params);

    }

    public function requestPosko(Request $request){

        $latawal=$request->latawal;
        $longawal=$request->longawal;

        $dataposko = Post::all();

        foreach ($dataposko as $item){
            $theta = $longawal-(float)$item->longitude;
            $miles = (sin(deg2rad($latawal)) * sin(deg2rad($item->latitude))) + (cos(deg2rad($latawal)) * cos(deg2rad($item->latitude)) * cos(deg2rad($theta)));
            $miles = acos($miles);
            $miles = rad2deg($miles);
            $miles = $miles * 60 * 1.1515;
            $kilometers = $miles * 1.609344;
            $data[]=[
                'nama_posko'=>$item->name,
                'latitude'=>$item->latitude,
                'longitude'=>$item->longitude,
                'jarak'=>$kilometers,
                'deskripsi'=>$item->deskripsi
            ];
        }

        foreach ($data as $key=>$item2){
            $jarakdata[]=[$item2["jarak"]];
            $jarakmin=min($jarakdata);
        }

        foreach ($data as $item3){
            if($item3["jarak"]==$jarakmin[0]){
                $data2[]=[
                    'name'=>$item3["nama_posko"],
                    'latitude'=>$item3["latitude"],
                    'longitude'=>$item3["longitude"],
                    'jarak'=>$item3["jarak"],
                    'deskripsi'=>$item3["deskripsi"]
                ];
            }
        }

        $params = [
            'code' => 200,
            'message' => 'Get Posko Evakuasi Success!',
            'jarak' => $jarakmin[0],
            'posko' => $data2,
        ];
        return response()->json($params);

    }
    

}
