<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\user;
use App\Http\Resources\UserResource;

class RegisterController extends Controller
{
    public function action(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
        ]);

        $user = User::create([
            'username' => $request->username, 
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'api_token' => Str::random(80),
            'role' => 3,
        ]);

        return (new UserResource($user))->additional([
                'token' => "$user->api_token",
                'message' => "Registrasi Berhasil",
        ]);
    }
}
