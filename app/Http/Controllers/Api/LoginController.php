<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\user;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function action(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $currentUser = Auth::user();

            return (new UserResource($currentUser))->additional([
                'token' => $currentUser->api_token,
                'message' => "Login Berhasil",
            ]);
        } else {
            return response()->json([
                'error' => 'Periksa kembali email dan password anda!',
            ], 401);
        }
    }
}
