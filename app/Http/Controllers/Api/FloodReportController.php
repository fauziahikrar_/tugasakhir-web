<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\FloodReport;
use App\User;

class FloodReportController extends Controller
{
    public function action (Request $request)
    {   
        $this->validate($request, [
            'tanggal' => 'required',
            'alamat' => 'required',
            'tinggi_genangan' => 'required',
            'foto' => 'required|image',
            'keterangan' => 'required',
        ]);

        $floodreport = FloodReport::create([
            'user_id' => auth()->user()->id, 
            'tanggal' => $request->tanggal,
            'alamat' => $request->alamat,
            'tinggi_genangan' => $request->tinggi_genangan,
            'keterangan' => $request->keterangan,
            'foto' => $request->file('foto')->store('floodreport-fotos'),
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'status' => "BARU",
        ]);


        return response()->json([
            "data" => $floodreport,
            "message" => "Laporan berhasil ditambahkan"
        ], 201);
    }
}
