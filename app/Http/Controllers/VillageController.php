<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class VillageController extends Controller
{

    public function __construct(){
        $this->middleware(function($request, $next){
            if(Gate::allows('manage-admin')) return $next($request);
            abort(403, 'Anda tidak memiliki cukup hak akses');
            });
        }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $villages = \App\Village::paginate(10);
        return view('villages.index', ['villages' => $villages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subdistricts = \App\Subdistrict::all();
        $params=[
            'subdistrict'=>$subdistricts
        ];
        return view('villages.create',$params);  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = \Validator::make($request->all(),[
            "name" => "required",
            "latitude" => "required",
            "longitude" => "required",
            ])->validate();

        $maxId = \App\Village::max('id');

        $new_village = new \App\Village;

        $new_village->id = $maxId + 1;
        $new_village->name = $request->get('name');
        $new_village->longitude = $request->get('longitude');
        $new_village->latitude = $request->get('latitude');

        $new_village->created_by = \Auth::user()->id;
        $new_village->subdistrict_id=$request->subdistricts ;
        $new_village->save();

        return redirect()->route('villages.index')->with('status', 'Data desa telah berhasil ditambah');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subdistricts = \App\Subdistrict::all();
        $params=[
            'subdistrict'=>$subdistricts
        ];

        $village_to_edit = \App\Village::findOrFail($id);
        
        return view('villages.edit', ['village' => $village_to_edit],$params);
   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->get('name');
        $longitude = $request->get('longitude');
        $latitude = $request->get('latitude');

        $village = \App\Village::findOrFail($id);

        $village->name = $name;
        $village->longitude = $longitude;
        $village->latitude = $latitude;

        $village->updated_by = \Auth::user()->id;
        $village->subdistrict_id=$request->subdistricts ;

        $village->save();
        
        return redirect()->route('villages.index', ['village' => $id])->with('status', 'Data desa telah berhasil diperbarui');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $village = \App\Village::findOrFail($id);

        $village->delete();

        return redirect()->route('villages.index')->with('status', 'Data desa berhasil diarsipkan');
   
    }

    public function trash(){
        $deleted_village = \App\Village::onlyTrashed()->paginate(10);
        
        return view('villages.trash', ['villages' => $deleted_village]);
    }

    public function restore($id){
        $village = \App\village::withTrashed()->findOrFail($id);
        
        if($village->trashed()){
            $village->restore();
        } else {
            return redirect()->route('villages.index')->with('status', 'Data desa tidak ada di trash');
        }

        return redirect()->route('villages.index')->with('status', 'Data desa telah berhasil direstore');
    }

    public function deletePermanent($id){
        $village = \App\Village::withTrashed()->findOrFail($id);
        
        if(!$village->trashed()){
            return redirect()->route('villages.index')->with('status', 'Data desa aktif, Tidak bisa menghapus permanen!');
        } else {
            $village->forceDelete();
            return redirect()->route('villages.index')->with('status', 'Data desa telah berhasil dihapus permanen');
        }
    }
}
