<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class FloodHistoryController extends Controller
{

    public function __construct(){
        $this->middleware(function($request, $next){
            if(Gate::allows('manage-admin')) return $next($request);
            abort(403, 'Anda tidak memiliki cukup hak akses');
            });
        }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->get('status');

        $keyword = $request->get('keyword') ? $request->get('keyword') : '';
        
        if($status){
            $floodhistories = \App\FloodHistory::where('status', strtoupper($status))->paginate(10);
        } else {
            $floodhistories = \App\FloodHistory::paginate(10);
        }

        return view('floodhistories.index', ['floodhistories' => $floodhistories]);

     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $villages = \App\Village::all();

        $params=[
            'village'=>$villages
        ];
        return view('floodhistories.create',$params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validation = \Validator::make($request->all(),[
            "tanggal" => "required|date",
            "kepala_keluarga" => "required",
            "jiwa" => "required",
            "rumah" => "required",
            "sekolah" => "required",
            "kantor_desa" => "required",
            "sawah" => "required",
            "jalan" => "required",
            "latitude" => "required",
            "longitude" => "required",
            ])->validate();

        $new_floodhistory = new \App\FloodHistory;
        $maxId = \App\FloodHistory::max('id');

        $new_floodhistory->id = $maxId + 1;
        $new_floodhistory->tanggal = $request->get('tanggal');
        $new_floodhistory->kepala_keluarga = $request->get('kepala_keluarga');
        $new_floodhistory->jiwa = $request->get('jiwa');
        $new_floodhistory->rumah = $request->get('rumah');
        $new_floodhistory->sekolah = $request->get('sekolah');
        $new_floodhistory->kantor_desa = $request->get('kantor_desa');
        $new_floodhistory->sawah = $request->get('sawah');
        $new_floodhistory->jalan = $request->get('jalan');
        $new_floodhistory->latitude = $request->get('latitude');
        $new_floodhistory->longitude = $request->get('longitude');
        $new_floodhistory->status = $request->get('status');
        $new_floodhistory->created_by = \Auth::user()->id;
        $new_floodhistory->village_id=$request->villages ;
        $new_floodhistory->save();

        return redirect()->route('floodhistories.index')->with('status', 'Data riwayat banjir telah berhasil ditambah');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $floodhistory = \App\FloodHistory::findOrFail($id);
        $map = \App\FloodHistory::latest()->limit(10)->get();

        return view('floodhistories.show', ['floodhistory' => $floodhistory], ['map' => $map]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $villages = \App\Village::all();
        
        $params=[
            'village'=>$villages
        ];

        $floodhistory_to_edit = \App\FloodHistory::findOrFail($id);
        
        return view('floodhistories.edit', ['floodhistory' => $floodhistory_to_edit],$params);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $floodhistory = \App\FloodHistory::findOrFail($id);
        
        $floodhistory->tanggal = $request->get('tanggal');
        $floodhistory->kepala_keluarga = $request->get('kepala_keluarga');
        $floodhistory->jiwa = $request->get('jiwa');
        $floodhistory->rumah = $request->get('rumah');
        $floodhistory->sekolah = $request->get('sekolah');
        $floodhistory->kantor_desa = $request->get('kantor_desa');
        $floodhistory->sawah = $request->get('sawah');
        $floodhistory->jalan = $request->get('jalan');
        $floodhistory->status = $request->get('status');
        $floodhistory->latitude = $request->get('latitude');
        $floodhistory->longitude = $request->get('longitude');
        $floodhistory->updated_by = \Auth::user()->id;
        $floodhistory->village_id=$request->villages ;

        $floodhistory->save();

        return redirect()->route('floodhistories.index', ['floodhistory' => $id])->with('status', 'Data Riwayat Banjir telah berhasil diperbarui');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $floodhistory = \App\FloodHistory::findOrFail($id);

        $floodhistory->delete();

        return redirect()->route('floodhistories.index')->with('status', 'Data Riwayat Banjir berhasil diarsipkan');

    }

    public function trash(){
        $deleted_floodhistory = \App\FloodHistory::onlyTrashed()->paginate(10);
        
        return view('floodhistories.trash', ['floodhistories' => $deleted_floodhistory]);
    }

    public function restore($id){
        $floodhistory = \App\FloodHistory::withTrashed()->findOrFail($id);
        
        if($floodhistory->trashed()){
            $floodhistory->restore();
        } else {
            return redirect()->route('floodhistories.index')->with('status', 'Data Riwayat Banjir tidak ada di trash');
        }

        return redirect()->route('floodhistories.index')->with('status', 'Data Riwayat Banjir telah berhasil direstore');
    }

    public function deletePermanent($id){
        $floodhistory = \App\FloodHistory::withTrashed()->findOrFail($id);
        
        if(!$floodhistory->trashed()){
            return redirect()->route('floodhistories.index')->with('status', 'Data Riwayat Banjir aktif, Tidak bisa hapus permanen!');
        } else {
            $village->forceDelete();
            return redirect()->route('floodhistories.index')->with('status', 'Data Riwayat Banjir telah berhasil dihapus permanen');
        }
    }
}
