<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PostController extends Controller
{

    public function __construct(){
        $this->middleware(function($request, $next){
            if(Gate::allows('manage-admin')) return $next($request);
            abort(403, 'Anda tidak memiliki cukup hak akses');
            });
        }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = \App\Post::paginate(10);

        return view('posts.index', ['posts' => $posts]);
 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $villages = \App\Village::all();

        $params=[
            'village'=>$villages
        ];
        return view('posts.create',$params);
  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = \Validator::make($request->all(),[
            "name" => "required",
            "alamat" => "required",
            "deskripsi" => "required",
            "latitude" => "required",
            "longitude" => "required",
            ])->validate();

        $maxId = \App\Post::max('id');
        $new_post = new \App\Post;
        $new_post->id = $maxId + 1;
        $new_post->name = $request->get('name');
        $new_post->alamat = $request->get('alamat');
        $new_post->deskripsi = $request->get('deskripsi');
        
        $new_post->latitude = $request->get('latitude');
        $new_post->longitude = $request->get('longitude');

        $new_post->created_by = \Auth::user()->id;
        $new_post->village_id=$request->villages ;
        $new_post->save();

        return redirect()->route('posts.index')->with('status', 'Data post telah berhasil ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = \App\Post::findOrFail($id);

        return view('posts.show', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $villages = \App\Village::all();
        
        $params=[
            'village'=>$villages
        ];

        $post_to_edit = \App\Post::findOrFail($id);
        
        return view('posts.edit', ['post' => $post_to_edit],$params);
  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = \App\Post::findOrFail($id);

        $post->name = $request->get('name');
        $post->alamat = $request->get('alamat');
        $post->deskripsi = $request->get('deskripsi');
        $post->latitude = $request->get('latitude');
        $post->longitude = $request->get('longitude');
     
        $new_foto = $request->file('foto');

        if($new_foto){
            if($post->foto && file_exists(storage_path('app/public/' . $post->foto))){
            \Storage::delete('public/'. $post->foto);
        }
        $new_foto_path = $new_foto->store('post-fotos', 'public');
        
        $post->foto = $new_foto_path;
        }

        $post->updated_by = \Auth::user()->id;
        $post->village_id=$request->villages ;

        $post->save();

        return redirect()->route('posts.index', ['id'=>$post->id])->with('status', 'Data posko telah berhasil diperbarui');
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = \App\Post::findOrFail($id);

        $post->delete();

        return redirect()->route('posts.index')->with('status', 'Data post telah diarsipkan');
  
    }

    public function trash(){
        $deleted_post = \App\Post::onlyTrashed()->paginate(10);
        
        return view('posts.trash', ['posts' => $deleted_post]);
    }

    public function restore($id){
        $post = \App\Post::withTrashed()->findOrFail($id);
        
        if($post->trashed()){
            $post->restore();
        } else {
            return redirect()->route('posts.index')->with('status', 'Data post tidak ada di tabel arsip');
        }

        return redirect()->route('posts.index')->with('status', 'Data post telah berhasil direstore');
    }

    public function deletePermanent($id){
        $post = \App\Post::withTrashed()->findOrFail($id);
        
        if(!$post->trashed()){
            return redirect()->route('posts.index')->with('status', 'Data post aktif, Tidak bisa hapus permanen!');
        } else {
            $post->forceDelete();
            return redirect()->route('posts.index')->with('status', 'Data post telah berhasil dihapus permanen');
        }
    }
}
