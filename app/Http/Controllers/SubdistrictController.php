<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SubdistrictController extends Controller
{

    public function __construct(){
        $this->middleware(function($request, $next){
            if(Gate::allows('manage-admin')) return $next($request);
            abort(403, 'Anda tidak memiliki cukup hak akses');
            });
        }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subdistricts = \App\Subdistrict::paginate(10);
        
        return view('subdistricts.index', ['subdistricts' => $subdistricts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subdistricts.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validation = \Validator::make($request->all(),[
            "name" => "required",
            "latitude" => "required",
            "longitude" => "required",
            ])->validate();

        $maxId = \App\Subdistrict::max('id');

        $new_subdistrict = new \App\Subdistrict;

        $new_subdistrict->id = $maxId + 1;
        $new_subdistrict->name = $request->get('name');
        $new_subdistrict->longitude = $request->get('longitude');
        $new_subdistrict->latitude = $request->get('latitude');

        $new_subdistrict->created_by = \Auth::user()->id;

        $new_subdistrict->save();

        return redirect()->route('subdistricts.index')->with('status', 'Data kecamatan telah berhasil ditambah'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subdistrict_to_edit = \App\Subdistrict::findOrFail($id);
        
        return view('subdistricts.edit', ['subdistrict' => $subdistrict_to_edit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->get('name');
        $longitude = $request->get('longitude');
        $latitude = $request->get('latitude');

        $subdistrict = \App\Subdistrict::findOrFail($id);

        $subdistrict->name = $name;
        $subdistrict->longitude = $longitude;
        $subdistrict->latitude = $latitude;

        $subdistrict->updated_by = \Auth::user()->id;

        $subdistrict->save();
        
        return redirect()->route('subdistricts.index', ['subdistrict' => $id])->with('status', 'Data Kecamatan telah berhasil diedit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subdistrict = \App\Subdistrict::findOrFail($id);

        $subdistrict->delete();

        return redirect()->route('subdistricts.index')->with('status', 'Data kecamatan berhasil diarsipkan');

    }

    public function trash(){
        $deleted_subdistrict = \App\Subdistrict::onlyTrashed()->paginate(10);
        
        return view('subdistricts.trash', ['subdistricts' => $deleted_subdistrict]);
    }

    public function restore($id){
        $subdistrict = \App\Subdistrict::withTrashed()->findOrFail($id);
        
        if($subdistrict->trashed()){
            $subdistrict->restore();
        } else {
            return redirect()->route('subdistricts.index')->with('status', 'Data kecamatan tidak ada di trash');
        }

        return redirect()->route('subdistricts.trash')->with('status', 'Data kecamatan telah berhasil direstore');
    }

    public function deletePermanent($id){
        $subdistrict = \App\Subdistrict::withTrashed()->findOrFail($id);
        
        if(!$subdistrict->trashed()){
            return redirect()->route('subdistricts.index')->with('status', 'Data kecamatan aktif, Tidak bisa hapus permanen!');
        } else {
            $subdistrict->forceDelete();
            return redirect()->route('subdistricts.index')->with('status', 'Data kecamatan telah berhasil dihapus permanen');
        }
    }

   
}
