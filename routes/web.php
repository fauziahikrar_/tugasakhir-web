<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/admin', 'AdminController@index')->name('admin')->middleware('admin');
Route::get('/operator', 'OperatorController@index')->name('operator')->middleware('operator');
Route::get('/lapor', 'LaporController@index')->name('lapor')->middleware('lapor');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource("users", "UserController");

Route::get('/subdistricts/trash', 'SubdistrictController@trash')->name('subdistricts.trash');
Route::get('/subdistricts/{id}/restore', 'SubdistrictController@restore')->name('subdistricts.restore');
Route::delete('/subdistricts/{id}/delete-permanent','SubdistrictController@deletePermanent')->name('subdistricts.delete-permanent');
Route::get('/ajax/subdistricts/search','SubdistrictController@ajaxSearch');
Route::resource("subdistricts", "SubdistrictController");

Route::get('/cari', 'Select2Controller@loadData');
Route::get('/villages/trash', 'VillageController@trash')->name('villages.trash');
Route::get('/villages/{id}/restore', 'VillageController@restore')->name('villages.restore');
Route::delete('/villages/{id}/delete-permanent','VillageController@deletePermanent')->name('villages.delete-permanent');
Route::resource('villages', 'VillageController');

Route::get('/floodreports/trash', 'FloodreportController@trash')->name('floodreports.trash');
Route::get('/floodreports/{id}/restore', 'FloodreportController@restore')->name('floodreports.restore');
Route::delete('/floodreports/{id}/delete-permanent','FloodreportController@deletePermanent')->name('floodreports.delete-permanent');
Route::resource('floodreports', 'FloodreportController');

Route::get('/helpreports/trash', 'HelpreportController@trash')->name('helpreports.trash');
Route::get('/helpreports/{id}/restore', 'HelpreportController@restore')->name('helpreports.restore');
Route::delete('/helpreports/{id}/delete-permanent','HelpreportController@deletePermanent')->name('helpreports.delete-permanent');
Route::resource('helpreports', 'HelpreportController');

Route::get('/floodhistories/trash', 'FloodhistoryController@trash')->name('floodhistories.trash');
Route::get('/floodhistories/{id}/restore', 'FloodhistoryController@restore')->name('floodhistories.restore');
Route::delete('/floodhistories/{id}/delete-permanent','FloodhistoryController@deletePermanent')->name('floodhistories.delete-permanent');
Route::resource('floodhistories', 'FloodhistoryController');

Route::get('/posts/trash', 'PostController@trash')->name('posts.trash');
Route::get('/posts/{id}/restore', 'PostController@restore')->name('posts.restore');
Route::delete('/posts/{id}/delete-permanent','PostController@deletePermanent')->name('posts.delete-permanent');
Route::resource('posts', 'PostController');