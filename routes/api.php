<?php

Route::post('register', 'Api\RegisterController@action');
Route::post('login', 'Api\LoginController@action');
Route::post('lapor-banjir', 'Api\FloodReportController@action')->middleware('auth:api');
Route::post('lapor-bantuan', 'Api\HelpReportController@action')->middleware('auth:api');
Route::get('posko', 'Api\PostController@action');
Route::post('cari-posko', 'Api\PostController@requestPosko');
Route::get('banjir-baru', 'Api\FloodHistoriesController@action');